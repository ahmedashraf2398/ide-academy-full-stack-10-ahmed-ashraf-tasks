'use strict';
 /* Day 7: Regular Expressions I */

 function regexVar() {
    
    var re = RegExp(/^([aeiou]).*\1$/);
    return re ;
}

/*END*/
   
/*Day 7: Regular Expressions II*/

function regexVar(){

const re = new RegExp(/^(Mr\.|Dr\.|Er\.|Ms\.|Mrs\.)\s?[a-z|A-Z]+$/);

return re;

}
/*Day 7: Regular Expressions III*/

function regexVar() {
   
   var re = RegExp('\\d+', 'g');

   return re;
}